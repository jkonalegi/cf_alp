require 'crowdflower'

module CfJobService
  API_KEY = ENV.fetch('CF_API_KEY').freeze
  DOMAIN_BASE = ENV.fetch('CF_API_HOST').freeze
  DEFAULT_BUCKET = 'crowdflower-alp-demo'

  class Base
    def connect_to_cf
      CrowdFlower::Job.connect! API_KEY, DOMAIN_BASE
    end

    def aws_s3_client
      @aws_s3_client ||= Aws::S3::Client.new
    end
  end
end