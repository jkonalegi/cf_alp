class JobsController < ApplicationController
  def index
  end

  def create
    JobCreationWorker.perform_async(params[:jobs].as_json)
    redirect_to root_path, flash: { success: 'Job created' }
  end
end