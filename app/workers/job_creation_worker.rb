# frozen_string_literal: true

class JobCreationWorker
  include Sidekiq::Worker

  sidekiq_options retry: false

  def perform(params)
    cf = CfJobService::CreateJob.new(path: params['path'], bucket: params['bucket'])
    cf.create_job
  end
end
