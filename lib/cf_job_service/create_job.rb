module CfJobService
  class CreateJob < Base
    attr_reader :path, :bucket

    def initialize(bucket: DEFAULT_BUCKET, path: nil)
      @path = path
      @bucket = bucket
      connect_to_cf
    end

    def create_job
      Rails.logger.info("Creating job for bucket: #{bucket} and path: #{path}")
      job = CrowdFlower::Job.create("CF-ALP #{path}")
      job.update(job_settings)

      # Create units
      unit_manager = CrowdFlower::Unit.new(job)
      public_urls.each { |url| unit_manager.create(image_url: url) }

      # launch jobs
      order_manager = CrowdFlower::Order.new(job)
      order_manager.debit(public_urls.count, ['cf_internal'])

      $redis.set("#{job.id}_bucket", bucket)
      $redis.set("#{job.id}_path", path)
    end

    private

    def public_urls
      @public_urls ||= list_files.map(&method(:convert_to_s3_object))
                                 .select(&method(:filter_for_images))
                                 .map(&:public_url)
    end


    def job_settings
      {
        cml: read_design_file('cml.html'),
        js: read_design_file('js.js'),
        instructions: read_design_file('instructions.html'),
        css: read_design_file('css.css'),
        project_number: 'PN123',
        webhook_uri: webhook_uri,
        judgments_per_unit: 2
      }
    end

    def webhook_uri
      uri = ENV.fetch('WEBHOOK_HOST')
      return if uri.blank?
      "#{uri}/webhook"
    end

    def read_design_file(file)
      File.read("#{Rails.root}/lib/cf_job_service/design/#{file}")
    end

    def filter_for_images(file)
      file.content_type.starts_with?('image/')
    end

    def convert_to_s3_object(file)
      Aws::S3::Object.new(client: aws_s3_client, bucket_name: bucket, key: file.key)
    end

    def list_files
      aws_s3_client.list_objects(bucket: bucket, prefix: path).contents
    end
  end
end