# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary server in each group
# is considered to be the first unless any hosts have the primary
# property set.  Don't declare `role :all`, it's a meta role.

deploy_auth = %w{ deployer@ec2-34-240-12-14.eu-west-1.compute.amazonaws.com }

role :app, deploy_auth
role :web, deploy_auth

set :rails_env, 'production'
set :bundle_jobs, 4

set :rvm_type, :user
set :rvm_ruby_version, '2.4.2@cf_alp'
set :branch, :master

# Default value for keep_releases is 5
set :keep_releases, 5
# puma settings
# set :puma_conf, "#{deploy_to}/current/config/puma/#{fetch(:rails_env)}.rb"
# set :sidekiq_config, "#{deploy_to}/current/config/sidekiq.yml"
# Extended Server Syntax
# ======================
# This can be used to drop a more detailed server definition into the
# server list. The second argument is a, or duck-types, Hash and is
# used to set extended properties on the server.

#server 'example.com', user: 'deploy', roles: %w{web app}, my_property: :my_value

# Custom SSH Options
# ==================
# You may pass any option but keep in mind that net/ssh understands a
# limited set of options, consult[net/ssh documentation](http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start).
#
# Global options
# --------------
set :ssh_options, {
  #keys: %w(/home/rlisowski/.ssh/id_rsa),
  forward_agent: true,
  #auth_methods: %w(password)
}

# And/or per server (overrides global)
# ------------------------------------
# server 'example.com',
#   user: 'user_name',
#   roles: %w{web app},
#   ssh_options: {
#     user: 'user_name', # overrides user setting above
#     keys: %w(/home/user_name/.ssh/id_rsa),
#     forward_agent: false,
#     auth_methods: %w(publickey password)
#     # password: 'please use keys'
#   }
