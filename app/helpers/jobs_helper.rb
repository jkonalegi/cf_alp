module JobsHelper
  def default_bucket
    ::CfJobService::DEFAULT_BUCKET
  end

  def base_line_models
    [
      OpenStruct.new(id: 1, name: 'Object Detection for Self Driving Cars')
    ]
  end

  def use_cases
    [
      OpenStruct.new(id: 1, name: 'Single Shot Detector')
    ]
  end
end
