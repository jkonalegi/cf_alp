# config valid only for Capistrano 3.1
lock '3.10.0'
application = 'cf_alp'
set :application, application
set :repo_url, 'git@bitbucket.org:jkonalegi/cf_alp.git'


# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app
set :deploy_to, '/home/deployer/apps/' + application
# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
set :log_level, :info

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, %w{ config/application.yml }

# Default value for linked_dirs is []
set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets }

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

namespace :deploy do

  task :stop do
    invoke 'puma:monit:unmonitor'
    invoke 'puma:stop'

    # invoke 'sidekiq:monit:unmonitor'
    # invoke 'sidekiq:stop'
  end

  task :start do
    invoke 'puma:monit:monitor'
    invoke 'puma:start'

    # invoke 'sidekiq:monit:monitor'
    # invoke 'sidekiq:start'
  end

  task :restart do
    invoke 'deploy:stop'
    invoke 'deploy:start'
  end

#  desc 'Restart application'
#  task :restart do
#    on roles(:app), in: :sequence, wait: 5 do
#      # Your restart mechanism here, for example:
#      # execute :touch, release_path.join('tmp/restart.txt')
#    end
#  end

#  after :publishing, :restart

#  after :restart, :clear_cache do
#    on roles(:web), in: :groups, limit: 3, wait: 10 do
#      # Here we can do anything such as:
#      # within release_path do
#      #   execute :rake, 'cache:clear'
#      # end
#    end
#  end

end

after 'deploy:publishing', 'deploy:restart'

# namespace :deploy do
#   task :restart do
#     invoke 'unicorn:restart'
#   end
# end
