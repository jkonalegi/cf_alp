class WebhooksController < ApplicationController
  skip_before_action :verify_authenticity_token

  def create
    JobReportWorker.perform_async(params.as_json)
  end
end