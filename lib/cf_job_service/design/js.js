var MAX_WIDTH=1024
document.querySelectorAll('.shape-display').forEach(function(shapeEl){
  var shape = JSON.parse(shapeEl.dataset.shapes).shapes[0]
  shapeEl.style.width = MAX_WIDTH + "px"
  shapeEl.insertAdjacentHTML('beforeend', '<div class="box"></div>')
  var box = shapeEl.querySelector('.box')
  var img = new Image()
  img.onload = function() {
    var ratio = MAX_WIDTH / this.width
    box.style.width = shape.width * ratio + "px"
    box.style.height = shape.height * ratio + "px"
    box.style.top = shape.y * ratio + "px"
    box.style.left = shape.x * ratio + "px"
  }
  img.src = shapeEl.querySelector('img').src
})


//Require the latest version of jQuery and the Bootstrap popover plugin
require(['jquery-noconflict', 'bootstrap-tooltip', 'bootstrap-popover'], function (jQuery) {
  //Ensure MooTools is where it must be
  Window.implement('$', function (el, nc) {
    return document.id(el, nc, this.document);
  });

  var $ = window.jQuery;

  //Begin tooltip popovers: http://getbootstrap.com/2.3.2/javascript.html#popovers
  $('.pedestrian').popover({
    html:true,
    placement:'bottom',
    trigger:'hover',
    title:'Pedestrian:',
    content:'<img src="https://s3.amazonaws.com/crowdflower-make-cloud/images%2F1489112941817-Screen+Shot+2017-03-09+at+5.42.27+PM.png"/>',
  });

  $('.baby_arm').popover({
    html:true,
    placement:'bottom',
    trigger:'hover',
    title:'Baby in arm:',
    content:'<img src=""/>',
  });

  $('.baby_stroller').popover({
    html:true,
    placement:'bottom',
    trigger:'hover',
    title:'Baby in a stroller:',
    content:'<img src="https://s3.amazonaws.com/crowdflower-make-cloud/images%2F1489113843176-baby_stroller.png"/>',
  });

  $('.child').popover({
    html:true,
    placement:'bottom',
    trigger:'hover',
    title:'Child:',
    content:'<img src="https://s3.amazonaws.com/crowdflower-make-cloud/images%2F1489112150065-Screen+Shot+2017-03-09+at+6.15.23+PM.png"/>',
  });

  $('.bike_rider').popover({
    html:true,
    placement:'bottom',
    trigger:'hover',
    title:'Bicycle rider:',
    content:'<img src=""/>',
  });

  $('.motor_rider').popover({
    html:true,
    placement:'bottom',
    trigger:'hover',
    title:'Motorcycle rider:',
    content:'<img src=""/>',
  });

  $('.person_car').popover({
    html:true,
    placement:'bottom',
    trigger:'hover',
    title:'Person riding within a car:',
    content:'<img src=""/>',
  });

  $('.person_wheelchair').popover({
    html:true,
    placement:'bottom',
    trigger:'hover',
    title:'Person in a wheelchair:',
    content:'<img src=""/>',
  });

  $('.pet').popover({
    html:true,
    placement:'bottom',
    trigger:'hover',
    title:'Pet of any kind (dogs, cats):',
    content:'<img src="https://s3.amazonaws.com/crowdflower-make-cloud/images%2F1489113823703-pet.png"/>',
  });

  $('.wild_animal').popover({
    html:true,
    placement:'bottom',
    trigger:'hover',
    title:'Wild animal of any kind (squirrel, bird only if on road):',
    content:'<img src=""/>',
  });

});
