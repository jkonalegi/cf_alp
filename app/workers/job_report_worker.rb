# frozen_string_literal: true

class JobReportWorker
  include Sidekiq::Worker

  sidekiq_options retry: 5

  #  {"signal"=>"job_complete", "payload"=>"{\"id\":1195757,
  def perform(params)
    Rails.logger.info("Payload: #{params}" )
    return if params['signal'] != 'job_complete'
    parsed_payload = JSON.parse(params['payload'])
    CfJobService::Report.new(job_id: parsed_payload['id']).generate_and_upload
  end
end
