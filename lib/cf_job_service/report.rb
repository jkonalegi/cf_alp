require 'zip'

module CfJobService
  class Report < Base
    attr_reader :job_id

    def initialize(job_id:)
      @job_id = job_id
      connect_to_cf
    end

    def generate_and_upload
      download
      unzip
      upload
    rescue => ex
      p ex
      clean
    end

    private

    def clean
      remove_file(unziped_path)
      remove_file(download_path)
    end

    def download
      job = CrowdFlower::Job.new(job_id)
      job.download_csv(:aggregated, download_path)

      3.times do
        job.regenerate(type: :aggregated)
        sleep(10)
      end

      job.download_csv(:aggregated, download_path)
    end

    def upload
      aws_s3_client.put_object(
        bucket: bucket,
        key: s3_file_path,
        body: File.new(unziped_path),
        content_type: 'text/csv'
      )
    end

    def bucket
      $redis.get("#{job_id}_bucket")
    end

    def path
      $redis.get("#{job_id}_path")
    end

    def unzip
      Zip::File.open(download_path) do |zipfile|
        zipfile.each do |file|
          zipfile.extract(file, unziped_path)
        end
      end
    end

    def s3_file_path
      "#{path}/report.csv"
    end

    def remove_file(file)
      File.delete(file)
    end

    def unziped_path
      "#{Rails.root}/tmp/job_#{job_id}_aggregated_report.csv"
    end

    def download_path
      "#{Rails.root}/tmp/job_#{job_id}_aggregated_report.zip"
    end
  end
end