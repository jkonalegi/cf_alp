module CrowdFlower
  class Job < Base
    def download_csv(type = :full, filename = nil)
     filename ||= "#{type.to_s[0].chr}#{@id}.zip"
     res = connection.get("#{resource_uri}/#{@id}.csv", {:format => "zip", :query => {:type => type}})
     self.class.verify_response res
     puts "Status... #{res.code.inspect}"
     if res.code == 202
       puts "CSV Generating... Trying again in 10 seconds."
       Kernel.sleep 10
       download_csv(type, filename)
     else
       puts "CSV written to: #{File.expand_path(filename)}"
       File.open(filename, "wb") {|f| f.puts res.body }
     end
   end

   def regenerate(type: :full)
     connection.post("#{resource_uri}/#{@id}/regenerate", { body: { type: type } })
   end
  end
end
